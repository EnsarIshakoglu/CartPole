## CartPole

A pole is attached by an un-actuated joint to a cart, which moves along a frictionless track. The system is controlled by applying a force of +1 or -1 to the cart. The pendulum starts upright, and the goal is to prevent it from falling over. A reward of +1 is provided for every timestep that the pole remains upright. The episode ends when the pole is more than 15 degrees from vertical, or the cart moves more than 2.4 units from the center. (https://gym.openai.com/envs/CartPole-v0/)

## Observation

Another name for the observation is the state. The state consists of the following:
| Num  | Observation           | Min                  | Max                |
| :--- | :-------------------- | :------------------- | :----------------- |
| 0    | Cart Position         | -4.8                 | 4.8                |
| 1    | Cart Velocity         | -Inf                 | Inf                |
| 2    | Pole Angle            | -0.418 rad (-24 deg) | 0.418 rad (24 deg) |
| 3    | Pole Angular Velocity | -Inf                 | Inf                |

## Actions

The actions are the moves the cart pole is able to perform. The actions consist of the following:

| Num  | Action                 |
| :--- | :--------------------- |
| 0    | Push cart to the left  |
| 1    | Push cart to the right |

## Technique

This AI is going to made using the Deep Q-Learning (DQN) methodology. The reason for why DQN is being used instead of Q-Learning is because some values in the observation space have an infinite value. This would mean the Q-table would be infinitely large and it would take infinitely long to train the AI. 

By making use of DQN, we can use a Neural Network to approximate the Q-value function. The state is given as the input and the Q-value of all possible moves are generated as the output.

## Architecture

The architecture of this Neural Network is as follows: 

![DQN Architecture](images/Screenshot%202021-11-15%20151507.png)

The value L is the amount of layers and the value N is the amount of actions. In the cart pole example, N would be 2 as there are 2 actions. The parameter L can be seen as a hyperparameter which means different values of L are usually tested and the one fitting the model the best will be used.